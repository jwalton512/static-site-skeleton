"use strict";

const elixir = require('laravel-elixir');
const del = require('del');
const task = elixir.Task;

require('laravel-elixir-pug');

elixir.config.assetsPath = 'src';
elixir.config.publicPath = 'build';

elixir.extend('del', (path) => {
  new task('del', () => {
    return del(path);
  });
});

elixir((mix) => {
  mix.del('./build/')
    .sass('style.sass')
    .browserify(['scripts.js'])
    .pug({
      blade: false,
      pretty: true,
      src: 'src/pug',
      exclude: '_*.pug',
      dest: 'build',
      additional_watches: ['src/**/*.pug']
  });
});
